fun main() {
    print("How many operation would you like to do: ")
    val kol = readLine()!!.toInt()
    for(i in 1..kol){
        print("First digit: ")
        val number1 = readLine()!!.toInt()
        print("Second digit: ")
        val number2 = readLine()!!.toInt()
        print("Operation (+, -, *, /, special): ")
        val operation = readLine()
        when (operation){
            "+" -> println("$number1 + $number2 = ${number1 + number2}")
            "-" -> println("$number1 - $number2 = ${number1 - number2}")
            "*" -> println("$number1 * $number2 = ${number1 * number2}")
            "/" -> println("$number1 / $number2 = ${number1 / number2}")
            "volume" -> println("volume ($number1, $number2) = ${volume(number1,number2)}")
            "nod" -> println("Nod ($number1, $number2) = ${Nod(number1,number2)}")
            "nok" -> println("Nok ($number1, $number2) = ${Nok(number1,number2)}")
        }
    }
}

fun Nod (number1: Int, number2: Int):Int{
    var a: Int = number1;
    var b: Int = number2;
    while (a != b){
        if (a > b){ a = a - b}
        else {b = b - a}
    }
    return a;
}

fun Nok (number1: Int, number2: Int):Int{
    return (number1 * number2 / Nod(number1, number2))
}

fun volume (number1: Int, number2: Int):Int{
    return (Math.PI * number1 * number1 * number2).toInt();
}
